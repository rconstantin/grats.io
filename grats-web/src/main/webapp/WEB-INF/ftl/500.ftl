<#import "master/indexmaster.ftl" as layout /> 
<@layout.indexmaster title="500">
<!-- Main -->
<article id="main">

	<header class="special container">
		<span class="icon fa-exclamation-triangle"></span>
		<h2>
			Something went <strong>terribly wrong!</strong>
		</h2>
	</header>

	<!-- One -->
	<section class="wrapper style4 container special">
		We have been informed about the problem. Click <a href="#" onclick="window.history.back();">here</a> to go back.
	</section>


</article>
</@layout.indexmaster>