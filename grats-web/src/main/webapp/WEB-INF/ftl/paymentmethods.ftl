<#import "master/homemaster.ftl" as layout />
<#import "includes/formMacros.ftl" as macro /> 
<@layout.homemaster title="paymentmethods">
<div class="panel-heading">
	<h3 class="panel-title">
		Payment methods
		<ul class="rad-panel-action">
			<li><i class="fa fa-chevron-down"></i></li>
			<li><i class="fa fa-rotate-right"></i></li>
			<li><i class="fa fa-close"></i></li>
		</ul>
	</h3>
</div>
<div class="panel-body">
	<div class="row">
		<#if paymentMethodsFull?size gt 0>
			<#list paymentMethodsFull as pm>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<ul class="rad-panel-action">
							<li><i class="fa fa-pencil"></i></li>
							<li><i class="fa fa-rotate-right"></i></li>
							<li><i class="fa fa-close"></i></li>
						</ul>
					</div>
					<div class="panel-body">
						<@macro.textSimple path=pm.number?html messageKey="paymentmethods.number" id="number"/>
						<@macro.textSimple path=pm.expiry?html messageKey="paymentmethods.expiry" id="expiry"/>
						<@macro.textSimple path=pm.firstName?html messageKey="paymentmethods.firstname" id="firstname"/>
						<@macro.textSimple path=pm.surname?html messageKey="paymentmethods.surname" id="surname"/>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<ul class="rad-panel-action">
							<li><i class="fa fa-pencil"></i></li>
							<li><i class="fa fa-rotate-right"></i></li>
							<li><i class="fa fa-close"></i></li>
						</ul>
					</div>
					<div class="panel-body">
						<@macro.textSimple path=pm.number?html messageKey="paymentmethods.number" id="number"/>
						<@macro.textSimple path=pm.expiry?html messageKey="paymentmethods.expiry" id="expiry"/>
						<@macro.textSimple path=pm.firstName?html messageKey="paymentmethods.firstname" id="firstname"/>
						<@macro.textSimple path=pm.surname?html messageKey="paymentmethods.surname" id="surname"/>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<ul class="rad-panel-action">
							<li><i class="fa fa-pencil"></i></li>
							<li><i class="fa fa-rotate-right"></i></li>
							<li><i class="fa fa-close"></i></li>
						</ul>
					</div>
					<div class="panel-body">
						<@macro.textSimple path=pm.number?html messageKey="paymentmethods.number" id="number"/>
						<@macro.textSimple path=pm.expiry?html messageKey="paymentmethods.expiry" id="expiry"/>
						<@macro.textSimple path=pm.firstName?html messageKey="paymentmethods.firstname" id="firstname"/>
						<@macro.textSimple path=pm.surname?html messageKey="paymentmethods.surname" id="surname"/>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<ul class="rad-panel-action">
							<li><i class="fa fa-pencil"></i></li>
							<li><i class="fa fa-rotate-right"></i></li>
							<li><i class="fa fa-close"></i></li>
						</ul>
					</div>
					<div class="panel-body">
						<@macro.textSimple path=pm.number?html messageKey="paymentmethods.number" id="number"/>
						<@macro.textSimple path=pm.expiry?html messageKey="paymentmethods.expiry" id="expiry"/>
						<@macro.textSimple path=pm.firstName?html messageKey="paymentmethods.firstname" id="firstname"/>
						<@macro.textSimple path=pm.surname?html messageKey="paymentmethods.surname" id="surname"/>
					</div>
				</div>
			</div>
			</#list>
			<#else>
				<#include "includes/nothing.ftl">
			</#if>
	</div>
</div>
</@layout.homemaster>
