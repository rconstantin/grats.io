<#import "master/homemaster.ftl" as layout />
<#import "includes/formMacros.ftl" as macro />
<@layout.homemaster title="funds">
<#-- @ftlvariable name="form" type="ro.pentalog.grats.model.entity.funds.FundsInformationCreateForm" -->
<div class="panel-heading">
	<h3 class="panel-title">
		Add funds!
		<ul class="rad-panel-action">
			<li><i class="fa fa-chevron-down"></i></li>
			<li><i class="fa fa-rotate-right"></i></li>
			<li><i class="fa fa-close"></i></li>
		</ul>
	</h3>
</div>
<div class="panel-body">
	<#include "includes/messages.ftl">
	<form role="form" name="form" action="/funds" method="post">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
		<div class="col-md-offset-4">
			<div class="form-group">
				<label class="col-sm-2" for="coinsBalance">Grats coins
					balance:</label>
				<div class="col-sm-5">
					<p id="coinsBalance">${coinsBalance}</p>
				</div>
			</div>
			<div class="clear"></div>
			<div class="form-group">
				<label class="col-sm-2" for="range">Amount:</label>
				<div class="col-sm-5">
					<div id="range" class="cntr"></div>
					<input type="hidden" id="amount" name="amount"
						value="${form.amount}">
				</div>
			</div>
			<div class="clear"></div>
			<div class="form-group">
				<label class="col-sm-2" for="coinsGained">Grats coins
					gained:</label>
				<div class="col-sm-5">
					<p id="coinsGainedValue">0</p>
					<input type="hidden" id="coinsGained" name="coinsGained"
						value="${form.coinsGained}">
				</div>
			</div>
			<div class="clear"></div>
			<#if hasPaymentMethod>
			<div class="form-group"><@macro.singleSelect
				path="form.selectedPaymentMethod" options=paymentMethods
				messageKey="funds.paymentMethod"/></div>
			<div class="clear"></div>
			</#if>
			<#if !hasPaymentMethod>
			<div class="form-group"><@macro.checkbox path="form.saveCard"
				messageKey="funds.saveCard" id="saveCard"
				attributes="class='toggle'" /></div>
			<div class="clear"></div>
			</#if>
			<div class="form-group"><@macro.checkbox path="form.recurr"
				messageKey="funds.recurr" id="recurr" attributes="class='toggle'
				${hasPaymentMethod?then('', 'disabled')}" /></div>
			<div class="clear"></div>
		</div>
		<#if !hasPaymentMethod>
		<div class="form-container">
			<div class="card-wrapper"></div>
			<div class="clear"></div>
			<input id="input-field" type="text" name="streetAddress"
				required="required" autocomplete="on" maxlength="45"
				placeholder="Streed Address" value="${form.streetAddress}" /> <input
				id="column-left" type="text" name="city" required="required"
				autocomplete="on" maxlength="20" placeholder="City"
				value="${form.city}" /> <input id="column-right" type="text"
				name="zipCode" required="required" autocomplete="on"
				pattern="[0-9]*" maxlength="5" placeholder="ZIP code"
				value="${form.zipCode}" /> <input id="column-left" type="text"
				name="firstName" placeholder="First Name" value="${form.firstName}" />
			<input id="column-right" type="text" name="surname"
				placeholder="Surname" value="${form.surname}" /> <input
				id="input-field" type="text" name="number" placeholder="Card Number"
				value="${form.number}" /> <input id="column-left" type="text"
				name="expiry" placeholder="MM / YY" value="${form.expiry}" /> <input
				id="column-right" type="text" name="cvc" placeholder="CCV"
				value="${form.cvc}" />
			<div class="clear"></div>
		</div>
		</#if>
		<div class="clear"></div>
		<div class="align-center margin20">
			<input type="submit" class="special" value="Submit" />
		</div>
	</form>
</div>
</@layout.homemaster>
