<#import "master/indexmaster.ftl" as layout /> 
<@layout.indexmaster title="contact"> 
<#-- @ftlvariable name="_csrf" type="org.springframework.security.web.csrf.CsrfToken" --> 
<#-- @ftlvariable name="form" type="ro.pentalog.grats.model.entity.mail.MailCreateForm" -->
<!-- Main -->
<article id="main">

	<header class="special container">
		<span class="icon fa-envelope"></span>
	</header>

	<!-- One -->
	<section class="wrapper style4 special container 75%">

		<!-- Content -->
		<div class="content">
			<#if error??>
			<div class="notify-error">${error}</div>
			</#if>
			<#if success??>
			<div class="notify-success">${success}</div>
			</#if>
			<form role="form" name="form" action="/contact" method="post">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<div class="row 50%">
					<div class="6u 12u(mobile)">
						<input type="text" name="name" placeholder="Name"
							value="${form.name}" required />
					</div>
					<div class="6u 12u(mobile)">
						<input type="email" name="email" placeholder="Email"
							value="${form.email}" required />
					</div>
				</div>
				<div class="row 50%">
					<div class="12u">
						<input type="text" name="subject" placeholder="Subject"
							value="${form.subject}" required />
					</div>
				</div>
				<div class="row 50%">
					<div class="12u">
						<textarea name="message" placeholder="Message" rows="7" required>${form.message}</textarea>
					</div>
				</div>
				<div class="row">
					<div class="12u">
						<ul class="buttons">
							<li><input type="submit" class="special"
								value="Send Message" /></li>
						</ul>
					</div>
				</div>
			</form>
		</div>

	</section>

</article>
</@layout.indexmaster> 