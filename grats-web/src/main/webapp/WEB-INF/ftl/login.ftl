<#import "master/indexmaster.ftl" as layout />
<#import "/spring.ftl" as spring>
<@layout.indexmaster title="login">
<#-- @ftlvariable name="_csrf" type="org.springframework.security.web.csrf.CsrfToken" -->
<#-- @ftlvariable name="form" type="ro.pentalog.grats.model.entity.user.UserCreateForm" -->
<#-- @ftlvariable name="error" type="java.util.Optional <String>" -->
<!-- Main -->
<article id="main">
	<header class="special container">
		<span class="icon fa-pencil"></span>
	</header>
	<section class="wrapper style4 special container">
		<div class="social-icons">
			<div class="col_1_of_f span_1_of_f">
				<a href="#" id="facebook-login-btn">
					<ul class='facebook'>
						<i class="fa fa-facebook facebook-icon"> </i>
						<li>Signup with Facebook</li>
						<div class='clear'></div>
					</ul>
				</a>
			</div>
			<div class="col_1_of_f span_1_of_f">
				<a href="#">
					<ul class='twitter'>
						<i class="fa fa-twitter twitter-icon"> </i>
						<li>Signup with Twitter</li>
						<div class='clear'></div>
					</ul>
				</a>
			</div>
			<div class="clear"></div>
		</div>
		<div class="or-spacer" id="register">
			<div class="mask"></div>
			<span><i>or</i></span>
		</div>
		<h2>Signup with</h2>
		<#if errors?has_content>
		<div class="notify-error">
			<ul>
				<#list errors as error>
				<li>${error.defaultMessage}</li>
				</#list>
			</ul>
		</div>
		</#if>
		<form role="form" name="form" action="/register" method="post">
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			<div class="row 50%">
				<div class="6u 12u(mobile)">
					<input type="text" name="name" id="registerName" placeholder="Name"
						value="${form.name}" required />
				</div>
				<div class="6u 12u(mobile)">
					<input type="text" name="email" id="registerEmail"
						placeholder="Email" value="${form.email}" required />
				</div>
			</div>
			<div class="row 50%">
				<div class="12u">
					<input type="password" name="password" placeholder="Password"
						value="${form.password}" required
						title="Password min 8 characters. At least one UPPERCASE and one lowercase letter"
						pattern="(?=^.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" />
				</div>
				<div class="12u">
					<input type="password" name="passwordRepeated"
						placeholder="Password Again" value="${form.passwordRepeated}"
						required
						title="Password min 8 characters. At least one UPPERCASE and one lowercase letter"
						pattern="(?=^.{8,}$)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" />
				</div>
			</div>
			<h3>
				By creating an account, you agree to our <span class="term"><a
					href="#">Terms & Conditions</a></span>
			</h3>
			<div class="row">
				<div class="12u">
					<ul class="buttons">
						<li><input type="submit" class="special" value="Sign up" /></li>
					</ul>
				</div>
			</div>
			<div class="clear"></div>
		</form>
		<div class="or-spacer" id="login">
			<div class="mask"></div>
			<span><i>or</i></span>
		</div>
		<h2>Sign in with</h2>
		<#if error.isPresent()>
		<div class="notify-error">The email or password you have entered
			is invalid, try again.</div>
		</#if>
		<#if success??>
			<div class="notify-success">Your account has been created. You can sign-in now.</div>
		</#if>
		<form role="form" action="/login" method="post">
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />
			<div class="row 50%">
				<div class="12u">
					<input type="text" name="email" placeholder="Email" required />
				</div>
				<div class="12u">
					<input type="password" name="password" placeholder="Password"
						required />
				</div>
			</div>
			<div class="row">
				<div class="12u">
					<ul class="buttons">
						<li><input type="submit" class="special" value="Sign in" /></li>
					</ul>
				</div>
			</div>
			<div class="clear"></div>
		</form>
	</section>
</article>
</@layout.indexmaster> 