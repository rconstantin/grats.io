<#import "master/homemaster.ftl" as layout />
<@layout.homemaster title="home">
<#import "spring.ftl" as spring />
<#-- @ftlvariable name="currentUser" type="ro.pentalog.grats.model.entity.user.CurrentUser" -->
<div class="panel-heading">
	<h3 class="panel-title">
		Home
		<ul class="rad-panel-action">
			<li><i class="fa fa-chevron-down"></i></li>
			<li><i class="fa fa-rotate-right"></i></li>
			<li><i class="fa fa-close"></i></li>
		</ul>
	</h3>
</div>
<div class="panel-body">
	<#if coinsBalance?? && coinsBalance == 0>
	<div class="notify-warning">
		In order to show your gratitude you must add funds. Click <a
			href="funds" target="_self">here</a> to add funds.
	</div>
	</#if> <#if !profile??>
	<div class="notify-warning">
		In order to receive gratitude you must create your profile. Click <a
			href="profile" target="_self">here</a> to create your profile.
	</div>
	</#if>
	<#include "includes/nothing.ftl">
</div>
</@layout.homemaster>