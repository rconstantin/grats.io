<#import "master/homemaster.ftl" as layout />
<@layout.homemaster title="timeline">
		<div class="panel-heading">
			<h3 class="panel-title">
				Timeline
				<ul class="rad-panel-action">
					<li><i class="fa fa-chevron-down"></i></li>
					<li><i class="fa fa-rotate-right"></i></li>
					<li><i class="fa fa-close"></i></li>
				</ul>
			</h3>
		</div>
<div class="panel-body">
		<#if notifications?size gt 0>
			<section id="cd-timeline" class="cd-container">
				<#list notifications as notification>
					<div class="cd-timeline-block">
						<div class="cd-timeline-img cd-picture">
						<#if notification.fromProfile?? && notification.fromProfile.photo?has_content>
							<a href="profile/${notification.fromProfile.username}" target="_blank"><img class="rad-list-img"
								src="${notification.fromProfile.photo}"
								alt="Picture"></a>
						<#else>
							<img class="rad-list-img"
									src="/assets/images/anon.png"
									alt="Picture">
						</#if>
						</div>
			<!-- cd-timeline-img -->

			<div class="cd-timeline-content">
				<h2>Gratitude!</h2>
				<p>${notification.message}</p>
				<a href="#0" class="cd-read-more">Thank back</a>
				<span class="cd-date">${notification.prettyDate}</span>
			</div>
			<!-- cd-timeline-content -->
		</div>
		<!-- cd-timeline-block -->
				</#list>
			</section>
		<#else>
			<#include "includes/nothing.ftl">
		</#if>
	<!-- cd-timeline -->
</div>
</@layout.homemaster>