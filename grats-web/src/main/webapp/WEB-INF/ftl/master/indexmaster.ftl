<#macro indexmaster title="defaultTitle">
<html>
<head>
<title>Grats!</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta name="_csrf_header" content="${_csrf.headerName}" />
<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/login/css/login.css" />
<link rel="stylesheet" href="assets/cookie/css/cookie.css" />
<link rel="stylesheet" href="assets/css/notification.css">
<link rel="stylesheet" href="/assets/css/bs.css">
<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
</head>
<body class="${title}">
	<div id="page-wrapper">
		<#if title != '500'>
			<!-- Header -->
			<#include "headerindexmaster.ftl">
		</#if>
		<!-- Main -->
		<div id="content"><#nested /></div>
		<!-- Footer -->
		<#include "../includes/cookie.ftl">
		<#if title != '500'>
			<footer id="footer">
	
				<ul class="icons">
					<li><a href="#" class="icon circle fa-twitter"><span
							class="label">Twitter</span></a></li>
					<li><a href="#" class="icon circle fa-facebook"><span
							class="label">Facebook</span></a></li>
					<li><a href="#" class="icon circle fa-google-plus"><span
							class="label">Google+</span></a></li>
					<li><a href="#" class="icon circle fa-github"><span
							class="label">Github</span></a></li>
					<li><a href="#" class="icon circle fa-dribbble"><span
							class="label">Dribbble</span></a></li>
				</ul>
				<ul class="copyright">
					<li>&copy; Untitled</li>
					<li>Design: <a href="#">PentaSmart</a></li>
				</ul>
			</footer>
		</#if>
	</div>
	<!-- Scripts -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/jquery.dropotron.min.js"></script>
	<script src="assets/js/jquery.scrolly.min.js"></script>
	<script src="assets/js/jquery.scrollgress.min.js"></script>
	<script src="assets/js/skel.min.js"></script>
	<script src="assets/js/jquery.cookie.js"></script>
	<script src="assets/js/util.js"></script>
	<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
	<script src="assets/js/main.js"></script>
	<script src="assets/login/js/facebook.js"></script>
	<script src="assets/cookie/js/cookie.js"></script>
</body>
</html>
</#macro>
