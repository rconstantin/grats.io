<#if title == 'index'>
<header id="header" class="alt">
<#else>
<header id="header">
</#if>
	<h1 id="logo">
		<a href="index">Grats <span>a PentaSmart ideea</span></a>
	</h1>
	<nav id="nav">
		<ul>
			<li class="current"><a href="index">Welcome</a></li>
			<li><a href="contact">Contact</a></li>
			<li><a href="/login" class="button special">Sign Up</a></li>
		</ul>
	</nav>
</header>