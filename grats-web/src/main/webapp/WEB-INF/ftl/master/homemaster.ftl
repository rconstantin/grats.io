<#macro homemaster title="defaultTitle">
<#import "/spring.ftl" as spring/>
<#-- @ftlvariable name="_csrf" type="org.springframework.security.web.csrf.CsrfToken" -->
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Grats!</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/assets/home/css/reset.css">
<link rel='stylesheet prefetch'
	href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'>
<link rel='stylesheet prefetch'
	href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css'>
<link rel='stylesheet prefetch'
	href='http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css'>
<link rel="stylesheet" href="/assets/home/css/style.css">
<link rel="stylesheet" href="/assets/css/notification.css">
<link rel="stylesheet" href="/assets/css/bs.css">
<link rel="stylesheet" href="/assets/css/grats.css">
<link rel="stylesheet" href="/assets/slider/css/slider.css">
<#if title == 'funds'>
<link rel="stylesheet" href="assets/funds/css/funds.css">
</#if>
<#if title == 'profile'>
<link rel="stylesheet" href="/assets/profile/css/profile.css">
</#if>
<#if title == 'timeline'>
<link rel="stylesheet" href="/assets/timeline/css/timeline.css">
</#if>
</head>
<body>
	<section>
		<header>
			<nav class="rad-navigation">
				<div class="rad-logo-container">
					<a href="/home" class="rad-logo"><i class=" fa fa-gg"></i> Grats</a>
				</div>
				<div class="rad-top-nav-container">
					<ul class="pull-right links">
						<#if currentUser??>Hi ${currentUser.user.email} </#if>
						<li class="rad-dropdown no-color"><a class="rad-menu-item"
							href="#"><img class="rad-list-img sm-img"
								src="<#if profile?? && profile.photo?has_content>${profile.photo} <#else>/assets/images/anon.png </#if>"
								alt="me" /></a>
							<ul class="rad-dropmenu-item sm-menu">
								<li class="rad-notification-item"><a
									class="rad-notification-content lg-text" href="/profile"><i
										class="fa fa-edit"></i> My Profile</a></li>
								<li class="rad-notification-item"><a
									class="rad-notification-content lg-text" href="/logout"><i
										class="fa fa-power-off"></i> Sign out</a></li>
							</ul></li>
						<li class="rad-dropdown"><a class="rad-menu-item" href="#"><i
								class="fa fa-cog"></i></a>
							<ul class="rad-dropmenu-item  bg-menu sm-menu">
								<li class="rad-notification-item"><a
									class="rad-notification-content lg-text" href="/paymentmethods"><i
										class="fa fa-credit-card"></i>Payment Methods</a></li>
								<li class="rad-notification-item"><a
									class="rad-notification-content lg-text" href="#"><i
										class="fa fa-history"></i>Gratitude History</a></li>
								<li class="rad-notification-item"><a
									class="rad-notification-content lg-text" href="#"><i
										class="fa fa-university"></i>Bank information</a></li>
								<li class="rad-notification-item"><a
									class="rad-notification-content lg-text" href="#"><i
										class="fa fa-cogs"></i>Settings</a></li>
							</ul></li>
					</ul>
					<ul class="pull-right links">
						<li><a class="rad-menu-item" href="/funds">${coinsBalance}<i
								class="fa icon-money-bag"></i></a></li>
					</ul>
					<ul class="pull-right links">
						<li class="rad-dropdown"><a id="gratsBalance" class="rad-menu-item" href="#">${gratsBalance}<i
								class="fa icon-money-note"><#if unseenNotificationsSize gt 0><span class="rad-menu-badge rad-bg-success">${unseenNotificationsSize}</span></#if></i></a>
							<ul class="rad-dropmenu-item">
								<li class="rad-dropmenu-header"><a href="#">Received Grats</a></li>
								<#if unseenNotificationsSize == 0><li class="rad-notification-item text-left">You have not
									received any new grats :(.</li> </#if>
								<#list unseenNotifications as notification>
								<li class="rad-notification-item text-left <#if !notification.seen>unseen</#if>" >${notification.message}<br /><small>${notification.prettyDate}</small></li> </#list>
								<li class="rad-dropmenu-footer"><a href="timeline">See more</a></li>
							</ul></li>
					</ul>
				</div>
			</nav>
		</header>
	</section>
	<main>
	<section>
		<div class="rad-body-wrapper rad-nav-min">
			<div class="col-md-6 col-lg-12">
				<div class="panel panel-default"><#nested /></div>
			</div>
		</div>
	</section>
	</main>
	<script src="/assets/js/jquery.min.js"></script>
	<script
		src='http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js'></script>
	<script
		src='http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js'></script>
	<script src='http://code.jquery.com/ui/1.11.4/jquery-ui.js'></script>
	<script
		src='https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.3/jquery.slimscroll.min.js'></script>
	<script
		src='https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.8.0/lodash.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js'></script>
	<script src="/assets/home/js/index.js"></script>
	<script src="/assets/js/bootstrap.js"></script>
	<script type="text/javascript">
		var notificationId = '${currentUser.user.id}';
		var _csrf = '${_csrf.token}';
	</script>
	<#if title == 'funds'>
	<script
		src='https://s3-us-west-2.amazonaws.com/s.cdpn.io/121761/card.js'></script>
	<script
		src='https://s3-us-west-2.amazonaws.com/s.cdpn.io/121761/jquery.card.js'></script>
	<script src="/assets/funds/js/funds.js"></script>
	<script src="/assets/slider/js/slider.js"></script>
	</#if>
	<#if title == 'profile'>
	<script src="/assets/profile/js/profile.js"></script>
	</#if>
	<#if title == 'timeline'>
	<script src="/assets/timeline/js/timeline.js"></script>
	</#if>
</body>
</html>
</#macro>
