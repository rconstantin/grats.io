<form role="form" name="form" action="/grats" method="post">
	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
	<input type="hidden" name="gratsToId" value="${otherProfile.userId}" />
	<input type="hidden" name="gratsTo" value="${otherProfile.username}" />
	<div class="col-lg-10 col-lg-offset-1">
		<div class="portrait">
			<div class="bg-line"></div>
			<div class="body-portrait">
				<div class="box-note">
					<div id="slider-note" class="carousel slide" data-ride="carousel"
						data-interval="10000">
						<div class="carousel-inner">
							<div class="item active">${otherProfile.introduction}</div>
						</div>
					</div>
				</div>
				<div class="portrait-img-area">
					<img src="<#if otherProfile.photo?has_content>${otherProfile.photo} <#else>/assets/images/anon.png </#if>" alt="avatar"
						class="rad-list-img-big avatar">
					<div class="profile-links">
						<a href="" class="facebook" data-toggle="tooltip"
							data-placement="left" title="" data-original-title="Facebook"><i
							class="fa fa-facebook"></i></a> <a href="" class="twitter"
							data-toggle="tooltip" data-placement="right" title=""
							data-original-title="Twitter"><i class="fa fa-twitter"></i></a>
					</div>
				</div>
				<h1 class="name">
					${otherProfile.username} <small>${otherProfile.description}</small>
				</h1>
				<div class="align-center margin20">
					<input type="submit" class="special" value="Show Gratitude"<#if
					isMyProfile??>disabled</#if> />
				</div>
			</div>
		</div>
	</div>
</form>
