<#if profile??>
	<div class="notify-info">
		You can view your profile <a href="/profile/${profile.username}">here.</a>
	</div>
</#if>
<form role="form" name="form" action="/profile" method="post">
	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
	<h1>
		<#if !profile??> <@spring.message "profile.h1.createUserProfile" />
		<#else> 
			<@spring.message "profile.h1.updateUserProfile" /> 
		</#if> <br>
		<a href="#" id="choose-photo"><i class="fa fa-camera-retro fa-lg"></i></a>
		<input type="file" id="files" class="user-profile-photo" accept="image/jpg, image/jpeg, image/png"/>
		<@macro.textInput path="form.photo" id="filesValue" type="hidden"
		placeHolderOnly="true" /> <img id="image" class="rad-list-img-big"
			src="<#if (profile.photo)??>${profile.photo} <#else>assets/images/anon.png </#if>" />
	</h1>
	<div class="clear"></div>
	<div class="col-md-4 col-md-offset-4">
		<h4><@spring.message "profile.h4.required" /></h4>
		<div class="6u 12u(mobile)">
			<div class="col-md-13"><@macro.textInput path="form.username"
				placeHolder="profile.username.placeholder" placeHolderOnly="true"
				helperKey="profile.username.helper" /></div>
		</div>
		<div class="clear"></div>
		<div class="6u 12u(mobile)">
			<div class="col-md-5">
				<p><@spring.message "profile.usereal" /></p>
			</div>
			<@macro.checkbox path="form.useRealName" wrapClass="col-md-5
			col-md-push-5" attributes="class='toggle'" />
			<div class="clear"></div>
			<p class="small help-block"><@spring.message
				"profile.usereal.helper" /></p>
		</div>
		<div class="6u 12u(mobile)">
			<div class="col-md-13"><@macro.textInput
				path="form.introduction" placeHolder="profile.onephrase.placeholder"
				placeHolderOnly="true" helperKey="profile.onephrase.helper" /></div>
		</div>
		<div class="6u 12u(mobile)">
			<div class="12u"><@macro.textArea path="form.description"
				placeHolder="profile.description.placeholder" attributes="rows='7'
				draggable='false' translate='no'" /></div>
		</div>
		<h4>Optional</h4>
		<div class="6u 12u(mobile)">
			<div class="col-md-13">
				<input type="text" name="website" id="website" placeholder="Website" />
			</div>
		</div>
		<div class="6u 12u(mobile)">
			<div class="col-md-13">
				<input type="text" name="username" id="username"
					placeholder="More optionals" disabled />
				<p class="small help-block">More to come... out of ideas</p>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="align-center margin20">
		<input type="submit" class="special" value="Submit" />
	</div>
</form>