<#if success??>
	<div class="notify-success">${success}</div>
</#if> 
<#if error??>
	<div class="notify-error">${error}</div>
</#if>