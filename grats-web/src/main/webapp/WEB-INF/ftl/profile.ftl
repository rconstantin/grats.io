<#import "master/homemaster.ftl" as layout />
<#import "includes/formMacros.ftl" as macro />
<#import "/spring.ftl" as spring />
<@layout.homemaster title="profile">
<#-- @ftlvariable name="form" type="ro.pentalog.grats.model.entity.profile.ProfileCreateForm" -->
		<div class="panel-heading">
			<h3 class="panel-title">
				<@spring.message "profile.panel.title" />
				<ul class="rad-panel-action">
					<li><i class="fa fa-chevron-down"></i></li>
					<li><i class="fa fa-rotate-right"></i></li>
					<li><i class="fa fa-close"></i></li>
				</ul>
			</h3>
		</div>
		<div class="panel-body">
			<#include "includes/messages.ftl">
			<#if !displayOther??> 
				<#include "includes/profile/myprofile.ftl">
			<#else> 
				<#if isMyProfile??>
				<div class="notify-info">
					This is your profile. Click <a href="#"
						onclick="window.history.back();">here</a> to go back.
				</div>
				</#if> 
				<#if otherProfile??> 
					<#include "includes/profile/otherprofile.ftl"> 
				<#else>
					<div class="notify-error">
						Profile not found!. Click <a href="#"
							onclick="window.history.back();">here</a> to go back.
					</div>
				</#if> 
			</#if>
		</div>
</@layout.homemaster>
