/* Card.js plugin by Jesse Pollak. https://github.com/jessepollak/card */
(function() {
	$('#saveCard').click(function() {
		if (this.checked) {
			$("#recurr").removeAttr("disabled");
		} else {
			$("#recurr").attr("disabled", true);
		}
	});
	if ($('.card-wrapper').length) {
		$('form').card({
			container : '.card-wrapper',
			width : 280,

			formSelectors : {
				nameInput : 'input[name="firstName"], input[name="surname"]'
			}
		});
	}
}.call(this));