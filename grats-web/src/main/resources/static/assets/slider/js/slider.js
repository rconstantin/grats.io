(function() {
	$('#range').slider({
		range : 'min',
		min : 10,
		max : 100,
		value : 10,
		slide : function(e, ui) {
			$('#coinsGainedValue').html(ui.value*10);
			$('#coinsGained').val(ui.value*10);
			$('#amount').val(ui.value);
			return $('.ui-slider-handle').html(ui.value+'$');
		}
	});
	$('.ui-slider-handle').html('10$');
	$('#coinsGained').val('100');
	$('#amount').val('10');
	$('#coinsGainedValue').html('100');
}.call(this));