window.fbAsyncInit = function() {
	FB.init({
		appId : '604347329715742',
		cookie : true, // enable cookies to allow the server to access
		// the session
		xfbml : true, // parse social plugins on this page
		version : 'v2.2' // use version 2.2
	});
};

(function() {
	// If we've already installed the SDK, we're done
	if (document.getElementById('facebook-jssdk')) {
		return;
	}

	// Get the first script element, which we'll use to find the parent node
	var firstScriptElement = document.getElementsByTagName('script')[0];

	// Create a new script element and set its id
	var facebookJS = document.createElement('script');
	facebookJS.id = 'facebook-jssdk';

	// Set the new script's source to the source of the Facebook JS SDK
	facebookJS.src = '//connect.facebook.net/it_IT/all.js';

	// Insert the Facebook JS SDK into the DOM
	firstScriptElement.parentNode.insertBefore(facebookJS, firstScriptElement);
}());

$('#facebook-login-btn').click(function() {
	FB.login(function(response) {
		if (response.authResponse) {
			console.log(response);
			console.log('Authenticated!');
			populateFields();
			// location.reload(); //or do whatever you want
		} else {
			console.log('User cancelled login or did not fully authorize.');
		}
	}, {
		scope : 'email,public_profile'
	});
});

function populateFields() {
	FB.api('/me?fields=name,email', function(response) {
		console.log(response);
		$("#registerName").val(response.name);
		$("#registerEmail").val(response.email);
	});
}
