$(function() {
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
});

$(document).ready(function() {

	if ($.cookie('grCookie') !== 'confirmed') {
		showNotification();
	}

	$('#confirmedCookieNoteId').submit(function(event, xhr, options) {
		$.ajax({
			url : $("#confirmedCookieNoteId").attr("action"),
			type : "POST",
			success : function(data) {
				closeNotification();
			},
		});
		event.preventDefault();
	});
});

function closeNotification() {
	$('#cookie-discalimer').hide();
}

function showNotification() {
	$('#cookie-discalimer').show();
}