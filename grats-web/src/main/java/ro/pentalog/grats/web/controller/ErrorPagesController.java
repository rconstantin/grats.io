package ro.pentalog.grats.web.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorPagesController implements ErrorController {

	private final static Logger LOG = LoggerFactory.getLogger(ErrorPagesController.class);

	@RequestMapping(value = "/error")
	public void error(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		LOG.error(processError(req, resp));
		resp.sendRedirect("/500");
	}

	@Override
	public String getErrorPath() {
		return "/error";
	}

	private String processError(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// Analyze the servlet exception
		Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
		Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
		String servletName = (String) request.getAttribute("javax.servlet.error.servlet_name");
		if (servletName == null) {
			servletName = "Unknown";
		}
		String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri");
		if (requestUri == null) {
			requestUri = "Unknown";
		}

		// Set response content type
		response.setContentType("text/html");

		StringBuilder sb = new StringBuilder();
		sb.append("[Error occured: ");
		if (statusCode != 500) {
			sb.append("Error Details: ");
			sb.append("Status code: " + statusCode);
			sb.append(" Request URI: " + requestUri);
		} else {
			sb.append("Exception Details: ");
			sb.append("Servlet Name:" + servletName);
			sb.append("Exception Name:" + throwable.getClass().getName());
			sb.append("Request URI: " + requestUri);
			sb.append("Exception Message:" + throwable.getMessage());
		}
		sb.append("]");
		return sb.toString();
	}
}