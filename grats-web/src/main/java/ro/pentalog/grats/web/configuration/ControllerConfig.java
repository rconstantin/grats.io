package ro.pentalog.grats.web.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class ControllerConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addViewControllers(final ViewControllerRegistry registry) {
		registry.addViewController("/home").setViewName("home");
		registry.addViewController("/").setViewName("index");
		registry.addViewController("/hello").setViewName("hello");
		registry.addViewController("/login").setViewName("login");
		registry.addViewController("/funds").setViewName("funds");
		registry.addViewController("/profile").setViewName("profile");
		registry.addViewController("/paymentmethods").setViewName("paymentmethods");
		registry.addViewController("/timeline").setViewName("timeline");
		registry.addViewController("/500").setViewName("500");
	}

}
