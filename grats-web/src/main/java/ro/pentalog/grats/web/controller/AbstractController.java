package ro.pentalog.grats.web.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import com.google.common.collect.Lists;

import ro.pentalog.grats.model.entity.funds.PaymentMethod;
import ro.pentalog.grats.model.entity.funds.Wallet;
import ro.pentalog.grats.model.entity.notification.Notification;
import ro.pentalog.grats.model.entity.profile.Profile;
import ro.pentalog.grats.model.entity.user.CurrentUser;
import ro.pentalog.grats.service.coinsbalance.WalletService;
import ro.pentalog.grats.service.notification.NotificationService;
import ro.pentalog.grats.service.paymentmethod.PaymentMethodService;
import ro.pentalog.grats.service.profile.ProfileService;
import ro.pentalog.grats.web.common.CommonConstants.Controllers.AbstractConstants;

@Controller
public abstract class AbstractController {

	@Autowired
	private WalletService walletService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private PaymentMethodService paymentMethodService;

	@Autowired
	private NotificationService notificationService;

	public abstract String getPage(Model model);

	protected void populateModelWithEssentialData(final Model model) {
		getCurrentUser(model);
		getWallet(model);
		getCurrentProfile(model);
		getNotifications(model);
	}

	protected void getCurrentUser(final Model model) {
		final SecurityContext context = SecurityContextHolder.getContext();
		final Authentication authentication = context.getAuthentication();
		if (authentication != null) {
			model.addAttribute(AbstractConstants.CURENT_USER, authentication.getPrincipal());
		}
	}

	protected CurrentUser getCurrentUser() {
		final SecurityContext context = SecurityContextHolder.getContext();
		final Authentication authentication = context.getAuthentication();
		if (authentication != null) {
			return (CurrentUser) authentication.getPrincipal();
		}
		return null;
	}

	protected Wallet getWallet() {
		final Optional<Wallet> wallet = getWalletService().findOneByUserId(getCurrentUser().getId());
		if (wallet.isPresent()) {
			return wallet.get();
		}
		return null;
	}

	protected void getWallet(final Model model) {
		Integer coinsBalance = 0;
		Integer gratsBalance = 0;
		final Wallet wallet = getWallet();
		if (wallet != null) {
			coinsBalance = wallet.getCoinsBalance();
			gratsBalance = wallet.getGratsBalance();
		}
		model.addAttribute(AbstractConstants.COINS_BALANCE, coinsBalance);
		model.addAttribute(AbstractConstants.GRATS_BALANCE, gratsBalance);
	}

	protected void getPaymentMethodsCards(final Model model) {
		final Optional<Collection<PaymentMethod>> fundsInformation = getPaymentMethodService()
				.findByUserId(getCurrentUser().getId());
		model.addAttribute(AbstractConstants.HAS_PAYMENT_METHOD,
				fundsInformation.isPresent() ? !fundsInformation.get().isEmpty() : false);
		if (fundsInformation.isPresent()) {
			// TODO: should support payment method types such as credit card,
			// paypal, bank transfer
			final Map<String, String> paymentMethods = new HashMap<>();
			for (final PaymentMethod paymentMethod : fundsInformation.get()) {
				paymentMethods.put(paymentMethod.getId(), "Card expiring in " + paymentMethod.getExpiry());
			}
			model.addAttribute(AbstractConstants.PAYMENT_METHODS, paymentMethods);
		}
	}

	protected void getPaymentMethodsFull(final Model model) {
		final Optional<Collection<PaymentMethod>> fundsInformation = getPaymentMethodService()
				.findByUserId(getCurrentUser().getId());
		if (fundsInformation.isPresent()) {
			// TODO: should support payment method types such as credit card,
			// paypal, bank transfer
			model.addAttribute(AbstractConstants.PAYMENT_METHODS_FULL, fundsInformation.get());
		}
	}

	protected void getCurrentProfile(final Model model) {
		model.addAttribute(AbstractConstants.PROFILE, getCurrentProfile());
	}

	protected Profile getCurrentProfile() {
		final Optional<Profile> profile = getProfileService().findOneByUserId(getCurrentUser().getId());
		if (profile.isPresent()) {
			return profile.get();
		}
		return null;
	}

	protected Collection<Notification> getUnseenNotifications() {
		final Optional<Collection<Notification>> unseenNotifications = notificationService
				.findByUserId(getCurrentUser().getId(), 5);
		if (unseenNotifications.isPresent()) {
			return unseenNotifications.get();
		}
		return new ArrayList<Notification>();
	}

	protected Collection<Notification> getNotifications() {
		final Optional<Collection<Notification>> notifications = notificationService
				.findByUserId(getCurrentUser().getId());
		if (notifications.isPresent()) {
			return Lists.reverse((List<Notification>) notifications.get());
		}
		return new ArrayList<Notification>();
	}

	protected void getNotifications(final Model model) {
		final Collection<Notification> unseenNotifications = Lists.newArrayList(getUnseenNotifications());
		model.addAttribute("unseenNotifications", unseenNotifications);

		final Long unseenNotificationsSize = notificationService.countByUserIdAndSeen(getCurrentUser().getId(), false);
		model.addAttribute("unseenNotificationsSize", unseenNotificationsSize > 99 ? 99 : unseenNotificationsSize);
	}

	public ProfileService getProfileService() {
		return profileService;
	}

	public PaymentMethodService getPaymentMethodService() {
		return paymentMethodService;
	}

	public WalletService getWalletService() {
		return walletService;
	}

	public NotificationService getNotificationService() {
		return notificationService;
	}

}
