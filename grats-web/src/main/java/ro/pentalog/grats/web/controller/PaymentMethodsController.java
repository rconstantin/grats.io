package ro.pentalog.grats.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PaymentMethodsController extends AbstractController {

	@RequestMapping(value = "/paymentmethods", method = RequestMethod.GET)
	public String getPage(Model model) {
		populateModelWithEssentialData(model);
		getPaymentMethodsFull(model);
		return "paymentmethods";
	}

}
