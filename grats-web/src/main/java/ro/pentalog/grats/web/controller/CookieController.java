package ro.pentalog.grats.web.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import ro.pentalog.grats.web.common.CommonConstants.Controllers.CookieConstants;

@Controller
public class CookieController {

	private static final String CONFIRMED = "confirmed";

	@RequestMapping(value = CookieConstants.ENDPOINT, method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void changeCookieStatus(final Model model, final HttpServletRequest request,
			final HttpServletResponse response) {

		setCookie(response, CookieConstants.COOKIE_NOTE_COOKIE_NAME, CONFIRMED,
				CookieConstants.COOKIE_NOTE_COOKIE_COMMENT);

	}

	protected void setCookie(final HttpServletResponse response, final String cookieName, final String value,
			final String comment) {
		final Cookie cookie = new Cookie(cookieName, value);
		cookie.setComment(comment);
		cookie.setMaxAge(30 * 24 * 60 * 60); // 30 days
		response.addCookie(cookie);
	}

}
