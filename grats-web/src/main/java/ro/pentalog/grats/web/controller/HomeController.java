package ro.pentalog.grats.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController extends AbstractController {

	@RequestMapping({ "/", "/index" })
	public String getHomePage() {
		return "index";
	}

	@RequestMapping("/about")
	public String getAboutPage() {
		return "about";
	}

	@RequestMapping("/contact")
	public String getContactPage() {
		return "contact";
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String getPage(Model model) {
		populateModelWithEssentialData(model);
		return "home";
	}

}
