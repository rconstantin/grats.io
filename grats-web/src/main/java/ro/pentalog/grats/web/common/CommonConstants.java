package ro.pentalog.grats.web.common;

public class CommonConstants {

	public static class Controllers {
		public static class AbstractConstants {

			public static final String ERROR = "error";
			public static final String SUCCESS = "success";
			public static final String WENT_WRONG_ERROR = "Something went wrong.";

			public static final String REDIRECT = "redirect:";

			public static final String CURENT_USER = "currentUser";
			public static final String COINS_BALANCE = "coinsBalance";
			public static final String HAS_PAYMENT_METHOD = "hasPaymentMethod";
			public static final String PAYMENT_METHODS = "paymentMethods";
			public static final String PAYMENT_METHODS_FULL = "paymentMethodsFull";
			public static final String PROFILE = "profile";
			public static final String GRATS_BALANCE = "gratsBalance";
		}

		public static class ContactConstants {
			public static final String ENDPOINT = "contact";
			public static final String QUOTA_ERROR = "You've exceeded your contact mail quota!";
			public static final String MESSAGE_SUCCESS = "Your message has been sent.";
		}

		public static class CookieConstants {
			public static final String COOKIE_NOTE_COOKIE_NAME = "grCookie";
			public static final String COOKIE_NOTE_COOKIE_COMMENT = "cookie note shown";
			public static final String COOKIE_NOTE_CONFIRMED = "confirmedCookieNote";
			public static final String ENDPOINT = "acceptcookie";
		}

		public static class FundsConstants {
			public static final String GRATS_COINS_UPDATED = "Your Grats coins balance has been updated.";
		}

		public static class ProfileConstants {
			public static final String PROFILE_SUCCESS = "Your profile has been updated.";
			public static final String PROFILE_ERROR = "Something went wrong. Try again later.";
			public static final String PROFILE_EXISTS = "Username already exists!";
		}
		
		public static class GratitudeConstants{
			public static final String NO_FUNDS = "Insuficient grats coins!";
		}
	}

}
