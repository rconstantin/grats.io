package ro.pentalog.grats.web.controller;

import java.util.Collection;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ro.pentalog.grats.model.entity.mail.ContactMail;
import ro.pentalog.grats.model.entity.mail.ContactMailCreateForm;
import ro.pentalog.grats.service.mail.ContactMailService;
import ro.pentalog.grats.web.common.CommonConstants.Controllers.AbstractConstants;
import ro.pentalog.grats.web.common.CommonConstants.Controllers.ContactConstants;
import ro.pentalog.grats.web.common.CommonMessage;
import ro.pentalog.grats.web.common.CommonMessageFactory;

@Controller
public class ContactController {

	Logger LOG = LoggerFactory.getLogger(ContactController.class);

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private ContactMailService contactMailService;

	@Value("${defaultEmailTo}")
	private String defaultTo;

	@Value("${maxEmailsContact}")
	private String maxEmailsContact;

	@RequestMapping(value = ContactConstants.ENDPOINT, method = RequestMethod.GET)
	public String getContactPage(final Model model) {
		model.addAttribute("form", new ContactMailCreateForm());
		return ContactConstants.ENDPOINT;
	}

	@RequestMapping(value = ContactConstants.ENDPOINT, method = RequestMethod.POST)
	public String sendEmail(final Model model, @ModelAttribute("form") final ContactMailCreateForm form,
			final RedirectAttributes attr, final HttpServletRequest request, final HttpServletResponse response) {

		final ContactMail contactMail = prepareEmail(form, request);
		if (contactMail == null) {
			final CommonMessage error = CommonMessageFactory
					.createErrorMessage(ContactConstants.QUOTA_ERROR + maxEmailsContact);
			attr.addFlashAttribute(error.getType(), error.getMessage());
			return AbstractConstants.REDIRECT + ContactConstants.ENDPOINT;
		}
		return prepareSendMail(contactMail, attr);
	}

	private ContactMail prepareEmail(final ContactMailCreateForm form, final HttpServletRequest request) {
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		form.setIpAddress(ipAddress);

		final Collection<ContactMail> contactEmails = contactMailService.findByIpAddress(ipAddress);
		if (contactEmails.size() < Integer.parseInt(maxEmailsContact)) {
			return contactMailService.create(form);
		}
		return null;
	}

	private String prepareSendMail(final ContactMail form, final RedirectAttributes attr) {
		final MimeMessage mail = javaMailSender.createMimeMessage();
		try {
			final MimeMessageHelper helper = new MimeMessageHelper(mail, true);
			helper.setTo(defaultTo);
			helper.setFrom(form.getName() + "<" + form.getEmail() + ">");
			helper.setSubject(form.getSubject());
			helper.setText(form.getMessage());
			javaMailSender.send(mail);
			final CommonMessage success = CommonMessageFactory.createSuccessMessage(ContactConstants.MESSAGE_SUCCESS);
			attr.addFlashAttribute(success.getType(), success.getMessage());
		} catch (final Exception e) {
			LOG.error(e.getMessage());
			final CommonMessage error = CommonMessageFactory.createErrorMessage(AbstractConstants.WENT_WRONG_ERROR);
			attr.addFlashAttribute(error.getType(), error.getMessage());
		}
		return AbstractConstants.REDIRECT + ContactConstants.ENDPOINT;
	}

}
