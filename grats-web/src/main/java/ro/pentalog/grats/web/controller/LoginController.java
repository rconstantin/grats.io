package ro.pentalog.grats.web.controller;

import java.security.Principal;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ro.pentalog.grats.model.entity.user.UserCreateForm;
import ro.pentalog.grats.service.user.ApplicationUserService;
import ro.pentalog.grats.web.validator.user.UserCreateFormValidator;

@Controller
public class LoginController {

	private final static Logger LOG = LoggerFactory.getLogger(LoginController.class);

	private final ApplicationUserService userService;
	private final UserCreateFormValidator userCreateFormValidator;

	@Autowired
	public LoginController(ApplicationUserService userService, UserCreateFormValidator userCreateFormValidator) {
		this.userService = userService;
		this.userCreateFormValidator = userCreateFormValidator;
	}

	@InitBinder("form")
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(userCreateFormValidator);
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String getLoginPage(Model model, @RequestParam Optional<String> error, Principal principal) {
		if (principal != null && ((Authentication) principal).isAuthenticated()) {
			return "redirect:home";
		}
		if (!model.containsAttribute("form")) {
			model.addAttribute("form", new UserCreateForm());
		}
		model.addAttribute("error", error);
		return "login";

	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String handleUserCreateForm(@Valid @ModelAttribute("form") UserCreateForm form, BindingResult bindingResult,
			RedirectAttributes attr) {
		if (bindingResult.hasErrors()) {
			attr.addFlashAttribute("errors", bindingResult.getGlobalErrors());
			form.setPassword("");
			form.setPasswordRepeated("");
			attr.addFlashAttribute("form", form);
			LOG.error("[Error while registering {} - error: {} ]", form.getEmail(), bindingResult.getGlobalError());
			return "redirect:login#register";
		}
		try {
			userService.create(form);
			LOG.info("[Created user with email: {} ]", form.getEmail());
		} catch (DataIntegrityViolationException e) {
			bindingResult.reject("email.exists", "Email already exists");
			attr.addFlashAttribute("errors", bindingResult.getGlobalErrors());
			form.setPassword("");
			form.setPasswordRepeated("");
			form.setEmail("");
			attr.addFlashAttribute("form", form);
			LOG.error("[Error while registering {} - error: {} ]", form.getEmail(), bindingResult.getGlobalError());
			return "redirect:login#register";
		}
		attr.addFlashAttribute("success", true);
		return "redirect:login?success#login";
	}

}
