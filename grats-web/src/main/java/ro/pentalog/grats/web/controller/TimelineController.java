package ro.pentalog.grats.web.controller;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.google.common.collect.Lists;

import ro.pentalog.grats.model.entity.notification.Notification;
import ro.pentalog.grats.model.entity.profile.Profile;
import ro.pentalog.grats.service.notification.NotificationService;

@Controller
public class TimelineController extends AbstractController {

	@Autowired
	NotificationService notificationService;

	@RequestMapping(value = "/notificationseen", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void changeCookieStatus(@ModelAttribute("notificationId") String notificationId,
			final HttpServletRequest request, final HttpServletResponse response) {
		final List<Notification> unseenNotifications = Lists
				.newArrayList(notificationService.findByUserId(notificationId).get());
		for (final Notification unseenNotification : unseenNotifications) {
			unseenNotification.setSeen(true);
		}

		notificationService.createOrUpdate(unseenNotifications);
	}

	@Override
	@RequestMapping(value = "timeline", method = RequestMethod.GET)
	public String getPage(Model model) {
		populateModelWithEssentialData(model);
		getTimelineData(model);
		return "timeline";
	}

	private void getTimelineData(Model model) {
		final Collection<Notification> notifications = getNotifications();
		for (final Notification notification : notifications) {
			final Optional<Profile> profileFrom = getProfileService().findOneByUserId(notification.getFromId());
			if (profileFrom.isPresent()) {
				notification.setFromProfile(profileFrom.get());
			}
		}
		model.addAttribute("notifications", notifications);
	}

}
