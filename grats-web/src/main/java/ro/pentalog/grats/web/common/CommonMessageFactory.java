package ro.pentalog.grats.web.common;

import ro.pentalog.grats.web.common.CommonConstants.Controllers.AbstractConstants;

public class CommonMessageFactory {
	
	// TODO: i18n

	private CommonMessageFactory() {

	}

	public static CommonMessage createSuccessMessage(String message) {
		return createMessage(AbstractConstants.SUCCESS, message);
	}

	public static CommonMessage createErrorMessage(String message) {
		return createMessage(AbstractConstants.ERROR, message);
	}

	private static CommonMessage createMessage(String type, String message) {
		return new CommonMessage(type, message);
	}

}
