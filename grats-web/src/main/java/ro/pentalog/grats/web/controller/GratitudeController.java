package ro.pentalog.grats.web.controller;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ro.pentalog.grats.model.entity.notification.Notification;
import ro.pentalog.grats.model.entity.profile.Profile;
import ro.pentalog.grats.web.common.CommonConstants.Controllers.AbstractConstants;
import ro.pentalog.grats.web.common.CommonConstants.Controllers.GratitudeConstants;
import ro.pentalog.grats.web.common.CommonMessage;
import ro.pentalog.grats.web.common.CommonMessageFactory;

@Controller
public class GratitudeController extends AbstractController {

	private final static Logger LOG = LoggerFactory.getLogger(GratitudeController.class);

	@RequestMapping(value = "/grats", method = RequestMethod.POST)
	public String handleGiveGrats(@ModelAttribute("gratsToId") final String gratsToId,
			@ModelAttribute("gratsTo") final String gratsTo, final BindingResult bindingResult,
			final RedirectAttributes attr) {
		// TODO: notifications!
		LOG.info("[Trying to give grats from {} to {} ]", getCurrentUser().getId(), gratsToId);
		try {
			if (getWalletService().updateWallet(getCurrentUser().getId(), gratsToId) != null) {
				// TODO: make factory!
				final Profile currentProfile = getCurrentProfile();
				String from = null;
				if (currentProfile == null) {
					from = "an anonymous user";
				} else {
					from = getCurrentProfile().getUseRealName() ? getCurrentUser().getUser().getName()
							: getCurrentProfile().getUsername();
				}
				getNotificationService().createOrUpdate(new Notification(gratsToId, new Date(),
						"Received grattitude from " + from + ".", false, getCurrentUser().getId()));
				LOG.info("[Success giving grats from {} to {} ]", getCurrentUser().getId(), gratsToId);
				final CommonMessage success = CommonMessageFactory
						.createSuccessMessage("You have shown your gratitude to " + gratsTo + "!");
				attr.addFlashAttribute(success.getType(), success.getMessage());
			} else {
				final CommonMessage error = CommonMessageFactory.createErrorMessage(GratitudeConstants.NO_FUNDS);
				attr.addFlashAttribute(error.getType(), error.getMessage());
			}
		} catch (final Exception e) {
			LOG.error("[Error while giving grats from {} to {} - error: {} ]", getCurrentUser().getId(), gratsToId,
					e.getMessage());
			final CommonMessage error = CommonMessageFactory.createErrorMessage(AbstractConstants.WENT_WRONG_ERROR);
			attr.addFlashAttribute(error.getType(), error.getMessage());
			return "redirect:profile/" + gratsTo;
		}
		return "redirect:profile/" + gratsTo;
	}

	@Override
	public String getPage(final Model model) {
		return null;
	}

}
