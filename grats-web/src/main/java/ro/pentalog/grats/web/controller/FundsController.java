package ro.pentalog.grats.web.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ro.pentalog.grats.model.entity.funds.FundsInformationCreateForm;
import ro.pentalog.grats.model.entity.funds.PaymentMethod;
import ro.pentalog.grats.web.common.CommonConstants.Controllers.FundsConstants;
import ro.pentalog.grats.web.common.CommonMessage;
import ro.pentalog.grats.web.common.CommonMessageFactory;

@Controller
public class FundsController extends AbstractController {

	private final static Logger LOG = LoggerFactory.getLogger(FundsController.class);

	@RequestMapping(value = "/funds", method = RequestMethod.GET)
	public String getPage(Model model) {
		populateModelWithEssentialData(model);
		getPaymentMethodsCards(model);
		if (!model.containsAttribute("form")) {
			model.addAttribute("form", new FundsInformationCreateForm());
		}
		return "funds";
	}

	@RequestMapping(value = "/funds", method = RequestMethod.POST)
	public String handleFundsInformationCreateForm(@Valid @ModelAttribute("form") FundsInformationCreateForm form,
			@ModelAttribute("selectedPaymentMethod") PaymentMethod selectedPaymentMethod, BindingResult bindingResult,
			RedirectAttributes attr) {
		form.setUserId(getCurrentUser().getId());
		if ("on".equals(form.getSaveCard())) {
			getPaymentMethodService().create(form);
			LOG.info("[Created funds payment method for: {} ]", getCurrentUser().getId());
		}

		getWalletService().create(form);
		LOG.info("[Added {} coins for: {} ]", form.getCoinsGained(), getCurrentUser().getId());
		CommonMessage success = CommonMessageFactory.createSuccessMessage(FundsConstants.GRATS_COINS_UPDATED);
		attr.addFlashAttribute(success.getType(), success.getMessage());
		return "redirect:funds";
	}

}
