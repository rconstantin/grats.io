package ro.pentalog.grats.web.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ro.pentalog.grats.model.entity.profile.Profile;
import ro.pentalog.grats.model.entity.profile.ProfileCreateForm;
import ro.pentalog.grats.model.entity.user.ApplicationUser;
import ro.pentalog.grats.service.user.ApplicationUserService;
import ro.pentalog.grats.web.common.CommonConstants.Controllers.ProfileConstants;
import ro.pentalog.grats.web.common.CommonMessage;
import ro.pentalog.grats.web.common.CommonMessageFactory;

@Controller
public class ProfileController extends AbstractController {

	private final static Logger LOG = LoggerFactory.getLogger(ProfileController.class);

	@Autowired
	private ApplicationUserService userService;

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public String getPage(Model model) {
		populateModelWithEssentialData(model);
		populateModelWithProfileData(model);
		return "profile";
	}

	@RequestMapping(value = "/profile/{username}", method = RequestMethod.GET)
	public String getProfilePage(@PathVariable String username, Model model) {
		populateModelWithEssentialData(model);
		populateModelWithOtherProfileData(model, username);
		return "profile";
	}

	@RequestMapping(value = "/profile", method = RequestMethod.POST)
	public String handleFundsInformationCreateForm(@Valid @ModelAttribute("form") ProfileCreateForm form,
			BindingResult bindingResult, RedirectAttributes attr) {
		LOG.info("[Trying to create/update profile for {} ]", getCurrentUser().getId());
		form.setUserId(getCurrentUser().getId());
		try {
			Profile profileToSave = getProfileService().findOneByUsername(form.getUsername()).get();
			if (profileToSave != null && !profileToSave.getUserId().equals(getCurrentUser().getId())) {
				LOG.error("[Error creating/updating profile for {}  - error {} ]", getCurrentUser().getId(),
						ProfileConstants.PROFILE_EXISTS);
				CommonMessage error = CommonMessageFactory.createErrorMessage(ProfileConstants.PROFILE_EXISTS);
				attr.addFlashAttribute(error.getType(), error.getMessage());
				attr.addFlashAttribute("form",form);
			} else {
				getProfileService().create(form);
				LOG.info("[Created/updated profile for {} ]", getCurrentUser().getId());
				CommonMessage success = CommonMessageFactory.createSuccessMessage(ProfileConstants.PROFILE_SUCCESS);
				attr.addFlashAttribute(success.getType(), success.getMessage());
			}
		} catch (Exception e) {
			LOG.error("[Error creating/updating profile for {}  - error {} ]", getCurrentUser().getId(),
					e.getMessage());
			CommonMessage error = CommonMessageFactory.createErrorMessage(ProfileConstants.PROFILE_ERROR);
			attr.addFlashAttribute(error.getType(), error.getMessage());
		}
		return "redirect:profile";
	}

	private void populateModelWithProfileData(Model model) {
		Profile profile = getCurrentProfile();
		ProfileCreateForm form = new ProfileCreateForm();
		if (profile != null) {
			form.setPhoto(profile.getPhoto());
			form.setUsername(profile.getUsername());
			form.setUseRealName(profile.getUseRealName() ? "true" : "false");
			form.setIntroduction(profile.getIntroduction());
			form.setDescription(profile.getDescription());
		}
		if (!model.containsAttribute("form")) {
			model.addAttribute("form", form);
		}
	}

	private void populateModelWithOtherProfileData(Model model, String username) {
		model.addAttribute("displayOther", true);
		Optional<Profile> profile = getProfileService().findOneByUsername(username);
		if (profile.isPresent()) {
			model.addAttribute("otherProfile", profile.get());
			if (profile.get().getUseRealName()) {
				Optional<ApplicationUser> otherUser = userService.getUserById(profile.get().getUserId());
				model.addAttribute("realName", otherUser.get().getName());
			}
			if (getCurrentProfile() != null) {
				if (profile.get().getUsername().equals(getCurrentProfile().getUsername())) {
					model.addAttribute("isMyProfile", true);
				}
			}
		} else {
			model.addAttribute("otherProfileNotFound", true);
		}
	}

}
