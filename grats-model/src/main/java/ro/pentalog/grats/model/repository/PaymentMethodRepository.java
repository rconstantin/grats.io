package ro.pentalog.grats.model.repository;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import ro.pentalog.grats.model.entity.funds.PaymentMethod;

public interface PaymentMethodRepository extends MongoRepository<PaymentMethod, String> {
	public Optional<PaymentMethod> findOneByUserId(String userId);

	public Optional<Collection<PaymentMethod>> findByUserId(String userId);
}
