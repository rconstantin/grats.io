package ro.pentalog.grats.model.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import ro.pentalog.grats.model.entity.profile.Profile;

public interface ProfileRepository extends MongoRepository<Profile, String> {

	public Optional<Profile> findOneByUserId(String userId);

	public Optional<Profile> findOneByUsername(String username);

}
