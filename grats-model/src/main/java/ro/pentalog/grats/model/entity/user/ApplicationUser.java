package ro.pentalog.grats.model.entity.user;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
public class ApplicationUser {

	@Id
	private String id;

	private String name;
	private String email;
	private String passwordHash;
	private Role role;

	public ApplicationUser() {

	}

	public ApplicationUser(final String name, final String username, final String passwordHash, final Role role) {
		this.setName(name);
		this.setEmail(username);
		this.setPasswordHash(passwordHash);
		this.setRole(role);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "User{" + "id='" + getId() + "'" + ", email='" + email + "'" + ", password='" + passwordHash + "'"
				+ ", role='" + role.name() + "'" + '}';
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
