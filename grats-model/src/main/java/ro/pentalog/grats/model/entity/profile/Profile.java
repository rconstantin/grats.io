package ro.pentalog.grats.model.entity.profile;

import org.springframework.data.mongodb.core.mapping.Document;

import ro.pentalog.grats.model.entity.BaseEntity;

@Document(collection = "profile")
public class Profile extends BaseEntity {

	private String photo;
	private String username;
	private Boolean useRealName;
	private String introduction;
	private String description;

	public Profile(final String photo, final String username, final Boolean useRealName, final String introduction,
			final String description, final String userId) {
		super(userId);
		this.photo = photo;
		this.username = username;
		this.useRealName = useRealName;
		this.introduction = introduction;
		this.description = description;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public Boolean getUseRealName() {
		return useRealName;
	}

	public void setUseRealName(final Boolean useRealName) {
		this.useRealName = useRealName;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(final String introduction) {
		this.introduction = introduction;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(final String photo) {
		this.photo = photo;
	}

}
