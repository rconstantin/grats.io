package ro.pentalog.grats.model.repository;

import java.util.Collection;

import org.springframework.data.mongodb.repository.MongoRepository;

import ro.pentalog.grats.model.entity.mail.ContactMail;

public interface ContactMailRepository extends MongoRepository<ContactMail, String> {
	public Collection<ContactMail> findByIpAddress(String ipAddress);

}
