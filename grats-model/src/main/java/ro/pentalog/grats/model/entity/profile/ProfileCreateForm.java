package ro.pentalog.grats.model.entity.profile;

public class ProfileCreateForm {

	private String username = "";
	private String photo = "";
	private String useRealName = "";
	private String introduction = "";
	private String description = "";

	private String userId;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getUseRealName() {
		return useRealName;
	}

	public void setUseRealName(String useRealName) {
		this.useRealName = useRealName;
	}
}
