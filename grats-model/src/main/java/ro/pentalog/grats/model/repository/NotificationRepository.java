package ro.pentalog.grats.model.repository;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import ro.pentalog.grats.model.entity.notification.Notification;

public interface NotificationRepository extends PagingAndSortingRepository<Notification, String> {
	public Optional<Collection<Notification>> findByUserIdAndSeen(String userId, Boolean seen);

	public Optional<Page<Notification>> findByUserId(String userId, Pageable pageable);

	public Optional<Collection<Notification>> findByUserId(String userId);

	public Long countByUserIdAndSeen(String userId, Boolean seen);
}
