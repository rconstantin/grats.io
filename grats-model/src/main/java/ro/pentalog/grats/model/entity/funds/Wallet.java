package ro.pentalog.grats.model.entity.funds;

import org.springframework.data.mongodb.core.mapping.Document;

import ro.pentalog.grats.model.entity.BaseEntity;

@Document(collection = "wallet")
public class Wallet extends BaseEntity {

	private Integer coinsBalance;
	private Integer gratsBalance;

	public Wallet(Integer coinsBalance, Integer gratsBalance, String userId) {
		super(userId);
		this.coinsBalance = coinsBalance;
		this.gratsBalance = gratsBalance;
	}

	public Integer getCoinsBalance() {
		return coinsBalance;
	}

	public void setCoinsBalance(Integer coinsBalance) {
		this.coinsBalance = coinsBalance;
	}

	public Integer getGratsBalance() {
		return gratsBalance;
	}

	public void setGratsBalance(Integer gratsBalance) {
		this.gratsBalance = gratsBalance;
	}

}
