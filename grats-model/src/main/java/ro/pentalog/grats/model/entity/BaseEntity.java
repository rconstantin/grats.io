package ro.pentalog.grats.model.entity;

import org.springframework.data.annotation.Id;

public class BaseEntity {

	@Id
	private String id;

	private String userId;

	public BaseEntity() {

	}

	public BaseEntity(String userId) {
		super();
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
