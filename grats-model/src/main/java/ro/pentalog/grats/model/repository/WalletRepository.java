package ro.pentalog.grats.model.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import ro.pentalog.grats.model.entity.funds.Wallet;

public interface WalletRepository extends MongoRepository<Wallet, String> {
	public Optional<Wallet> findOneByUserId(String userId);
}
