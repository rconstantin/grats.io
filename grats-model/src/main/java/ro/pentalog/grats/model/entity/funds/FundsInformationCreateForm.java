package ro.pentalog.grats.model.entity.funds;

public class FundsInformationCreateForm {

	// TODO: validations?

	private Integer coinsBalance = 0;
	private Integer amount = 0;
	private Integer coinsGained = 0;
	private String saveCard = "";
	private String recurr = "";

	private String streetAddress = "";
	private String city = "";
	private String zipCode = "";
	private String firstName = "";
	private String surname = "";
	private String number = "";
	private String expiry = "";
	private String cvc = "";
	
	private String selectedPaymentMethod;

	private String userId;

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getExpiry() {
		return expiry;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

	public String getCvc() {
		return cvc;
	}

	public void setCvc(String cvc) {
		this.cvc = cvc;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getCoinsBalance() {
		return coinsBalance;
	}

	public void setCoinsBalance(Integer coinsBalance) {
		this.coinsBalance = coinsBalance;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getCoinsGained() {
		return coinsGained;
	}

	public void setCoinsGained(Integer coinsGained) {
		this.coinsGained = coinsGained;
	}

	public String getSaveCard() {
		return saveCard;
	}

	public void setSaveCard(String saveCard) {
		this.saveCard = saveCard;
	}

	public String getRecurr() {
		return recurr;
	}

	public void setRecurr(String recurr) {
		this.recurr = recurr;
	}

	public String getSelectedPaymentMethod() {
		return selectedPaymentMethod;
	}

	public void setSelectedPaymentMethod(String selectedPaymentMethod) {
		this.selectedPaymentMethod = selectedPaymentMethod;
	}

}
