package ro.pentalog.grats.model.entity.user;

public enum Role {
	USER, ADMIN
}