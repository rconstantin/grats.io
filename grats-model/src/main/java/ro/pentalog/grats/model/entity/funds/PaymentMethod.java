package ro.pentalog.grats.model.entity.funds;

import org.springframework.data.mongodb.core.mapping.Document;

import ro.pentalog.grats.model.entity.BaseEntity;

@Document(collection = "paymentMethod")
public class PaymentMethod extends BaseEntity {

	private String streetAddress;
	private String city;
	private String zipCode;
	private String firstName;
	private String surname;
	private String number;
	private String expiry;
	private String cvc;

	public PaymentMethod() {

	}

	public PaymentMethod(String streetAddress, String city, String zipCode, String firstName, String surname,
			String number, String expiry, String cvc, String userId) {
		super(userId);
		this.streetAddress = streetAddress;
		this.city = city;
		this.zipCode = zipCode;
		this.firstName = firstName;
		this.surname = surname;
		this.number = number;
		this.expiry = expiry;
		this.cvc = cvc;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getExpiry() {
		return expiry;
	}

	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}

	public String getCvc() {
		return cvc;
	}

	public void setCvc(String cvc) {
		this.cvc = cvc;
	}

}
