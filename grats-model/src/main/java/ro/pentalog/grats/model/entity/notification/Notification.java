package ro.pentalog.grats.model.entity.notification;

import java.util.Date;

import org.ocpsoft.prettytime.PrettyTime;
import org.springframework.data.mongodb.core.mapping.Document;

import ro.pentalog.grats.model.entity.BaseEntity;
import ro.pentalog.grats.model.entity.profile.Profile;

@Document(collection = "notification")
public class Notification extends BaseEntity {

	private Date date;
	private String message;
	private Boolean seen;
	private String fromId;
	private Profile fromProfile;

	public Notification(final String userId, final Date date, final String message, final Boolean seen,
			final String fromId) {
		super(userId);
		this.date = date;
		this.message = message;
		this.seen = seen;
		this.fromId = fromId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(final Date date) {
		this.date = date;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}

	public Boolean getSeen() {
		return seen;
	}

	public void setSeen(final Boolean seen) {
		this.seen = seen;
	}

	public String getFromId() {
		return fromId;
	}

	public void setFromId(final String fromId) {
		this.fromId = fromId;
	}

	public Profile getFromProfile() {
		return fromProfile;
	}

	public void setFromProfile(final Profile fromProfile) {
		this.fromProfile = fromProfile;
	}

	public String getPrettyDate() {
		final PrettyTime prettyTime = new PrettyTime();
		return prettyTime.format(date);
	}

}
