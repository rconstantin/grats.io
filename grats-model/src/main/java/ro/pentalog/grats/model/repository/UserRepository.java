package ro.pentalog.grats.model.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import ro.pentalog.grats.model.entity.user.ApplicationUser;

public interface UserRepository extends MongoRepository<ApplicationUser, String> {

	public Optional<ApplicationUser> findOneByEmail(String email);
}
