package ro.pentalog.grats.service.notification.impl;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ro.pentalog.grats.model.entity.notification.Notification;
import ro.pentalog.grats.model.repository.NotificationRepository;
import ro.pentalog.grats.model.repository.ProfileRepository;
import ro.pentalog.grats.service.notification.NotificationService;

@Service
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	private NotificationRepository notificationRepository;

	@Autowired
	private ProfileRepository profileRepository;

	@Override
	public Notification createOrUpdate(final Notification notification) {
		// TODO: do this when displaying timeline!
		// notification.setFromProfile(profileRepository.findOneByUserId(notification.getFromId()).get());
		return notificationRepository.save(notification);
	}

	@Override
	public Optional<Collection<Notification>> findByUserId(String userId) {
		// TODO Auto-generated method stub
		return notificationRepository.findByUserId(userId);
	}

	@Override
	public Optional<Collection<Notification>> findByUserId(String userId, int limit) {
		final PageRequest request = new PageRequest(0, limit, new Sort(Sort.Direction.DESC, "date"));
		return Optional.of(notificationRepository.findByUserId(userId, request).get().getContent());
	}

	@Override
	public Long countByUserIdAndSeen(String userId, Boolean seen) {
		return notificationRepository.countByUserIdAndSeen(userId, seen);
	}

	@Override
	public Iterable<Notification> createOrUpdate(Iterable<Notification> notification) {
		return notificationRepository.save(notification);
	}

}
