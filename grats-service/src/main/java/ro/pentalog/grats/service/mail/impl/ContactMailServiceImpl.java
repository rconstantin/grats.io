package ro.pentalog.grats.service.mail.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.pentalog.grats.model.entity.mail.ContactMail;
import ro.pentalog.grats.model.entity.mail.ContactMailCreateForm;
import ro.pentalog.grats.model.repository.ContactMailRepository;
import ro.pentalog.grats.service.mail.ContactMailService;

@Service
public class ContactMailServiceImpl implements ContactMailService {

	private final ContactMailRepository contactMailRepository;

	@Autowired
	public ContactMailServiceImpl(ContactMailRepository contactMailRepository) {
		this.contactMailRepository = contactMailRepository;
	}

	@Override
	public Collection<ContactMail> findByIpAddress(String ipAddress) {
		return contactMailRepository.findByIpAddress(ipAddress);
	}

	@Override
	public ContactMail create(ContactMailCreateForm form) {
		ContactMail contactMail = new ContactMail(form.getIpAddress(), form.getName(), form.getEmail(),
				form.getSubject(), form.getMessage());
		return contactMailRepository.save(contactMail);
	}

}
