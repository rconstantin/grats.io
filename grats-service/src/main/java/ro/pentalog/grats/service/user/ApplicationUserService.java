package ro.pentalog.grats.service.user;

import java.util.Collection;
import java.util.Optional;

import ro.pentalog.grats.model.entity.user.ApplicationUser;
import ro.pentalog.grats.model.entity.user.UserCreateForm;

public interface ApplicationUserService {
	
	Optional<ApplicationUser> getUserById(String id);

	Optional<ApplicationUser> getUserByEmail(String email);

	Collection<ApplicationUser> getAllUsers();

	ApplicationUser create(UserCreateForm form);

}
