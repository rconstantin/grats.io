package ro.pentalog.grats.service.mail;

import java.util.Collection;

import ro.pentalog.grats.model.entity.mail.ContactMail;
import ro.pentalog.grats.model.entity.mail.ContactMailCreateForm;

public interface ContactMailService {
	public Collection<ContactMail> findByIpAddress(String ipAddress);

	public ContactMail create(ContactMailCreateForm form);
}
