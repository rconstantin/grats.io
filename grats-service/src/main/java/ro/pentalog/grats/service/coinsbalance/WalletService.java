package ro.pentalog.grats.service.coinsbalance;

import java.util.Optional;

import ro.pentalog.grats.model.entity.funds.Wallet;
import ro.pentalog.grats.model.entity.funds.FundsInformationCreateForm;

public interface WalletService {

	public Wallet create(FundsInformationCreateForm form);

	public Optional<Wallet> findOneByUserId(String userId);
	
	public Wallet updateWallet(String userIdFrom, String userIdTo);

}
