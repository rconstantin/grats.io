package ro.pentalog.grats.service.user.impl;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import ro.pentalog.grats.model.entity.user.ApplicationUser;
import ro.pentalog.grats.model.entity.user.UserCreateForm;
import ro.pentalog.grats.model.repository.UserRepository;
import ro.pentalog.grats.service.user.ApplicationUserService;

@Service
public class ApplicationUserServiceImpl implements ApplicationUserService {

	private final UserRepository userRepository;

	@Autowired
	public ApplicationUserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public Optional<ApplicationUser> getUserById(String id) {
		return Optional.ofNullable(userRepository.findOne(id));
	}

	@Override
	public Optional<ApplicationUser> getUserByEmail(String email) {
		return userRepository.findOneByEmail(email);
	}

	@Override
	public Collection<ApplicationUser> getAllUsers() {
		return userRepository.findAll(new Sort("email"));
	}

	@Override
	public ApplicationUser create(UserCreateForm form) {
		ApplicationUser user = new ApplicationUser();
		user.setName(form.getName());
		user.setEmail(form.getEmail());
		user.setPasswordHash(new BCryptPasswordEncoder().encode(form.getPassword()));
		user.setRole(form.getRole());
		return userRepository.save(user);
	}

}
