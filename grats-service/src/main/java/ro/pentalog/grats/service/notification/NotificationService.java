package ro.pentalog.grats.service.notification;

import java.util.Collection;
import java.util.Optional;

import ro.pentalog.grats.model.entity.notification.Notification;

public interface NotificationService {

	public Optional<Collection<Notification>> findByUserId(String userId);

	public Optional<Collection<Notification>> findByUserId(String userId, int limit);

	public Notification createOrUpdate(Notification notification);

	public Iterable<Notification> createOrUpdate(Iterable<Notification> notification);

	public Long countByUserIdAndSeen(String userId, Boolean seen);

}
