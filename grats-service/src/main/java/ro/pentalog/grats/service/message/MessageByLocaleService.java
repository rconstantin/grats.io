package ro.pentalog.grats.service.message;

public interface MessageByLocaleService {
	public String getMessage(String id);
}
