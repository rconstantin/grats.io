package ro.pentalog.grats.service.profile;

import java.util.Optional;

import ro.pentalog.grats.model.entity.profile.Profile;
import ro.pentalog.grats.model.entity.profile.ProfileCreateForm;

public interface ProfileService {

	public Profile create(ProfileCreateForm form);

	public Optional<Profile> findOneByUserId(String userId);
	
	public Optional<Profile> findOneByUsername(String username);

}
