package ro.pentalog.grats.service.coinsbalance.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.pentalog.grats.model.entity.funds.FundsInformationCreateForm;
import ro.pentalog.grats.model.entity.funds.Wallet;
import ro.pentalog.grats.model.repository.WalletRepository;
import ro.pentalog.grats.service.coinsbalance.WalletService;

@Service
public class WalletServiceImpl implements WalletService {

	private final WalletRepository walletRepository;

	@Autowired
	public WalletServiceImpl(final WalletRepository coinsBalanceRepository) {
		this.walletRepository = coinsBalanceRepository;
	}

	@Override
	public Wallet create(final FundsInformationCreateForm form) {
		Wallet newCoinsBalance = null;
		final Optional<Wallet> oldCoinsBalance = walletRepository.findOneByUserId(form.getUserId());
		if (oldCoinsBalance.isPresent()) {
			newCoinsBalance = oldCoinsBalance.get();
			newCoinsBalance.setCoinsBalance(newCoinsBalance.getCoinsBalance() + form.getCoinsGained());
		} else {
			newCoinsBalance = new Wallet(form.getCoinsGained(), 0, form.getUserId());
		}
		return walletRepository.save(newCoinsBalance);
	}

	@Override
	public Optional<Wallet> findOneByUserId(final String userId) {
		return walletRepository.findOneByUserId(userId);
	}

	@Override
	public Wallet updateWallet(final String userIdFrom, final String userIdTo) {
		final Optional<Wallet> walletFrom = walletRepository.findOneByUserId(userIdFrom);
		if (walletFrom.isPresent() && walletFrom.get().getCoinsBalance() > 0) {
			final Optional<Wallet> oldWalletTo = walletRepository.findOneByUserId(userIdTo);
			walletFrom.get().setCoinsBalance(walletFrom.get().getCoinsBalance() - 1);
			walletRepository.save(walletFrom.get());
			if (oldWalletTo.isPresent()) {
				// TODO: coins with different values
				oldWalletTo.get().setGratsBalance(oldWalletTo.get().getGratsBalance() + 1);
				return walletRepository.save(oldWalletTo.get());
			} else {
				final Wallet newCoinsBalanceTo = new Wallet(0, 1, userIdTo);
				return walletRepository.save(newCoinsBalanceTo);
			}
		}
		return null;
	}

}
