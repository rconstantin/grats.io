package ro.pentalog.grats.service.paymentmethod.impl;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.pentalog.grats.model.entity.funds.FundsInformationCreateForm;
import ro.pentalog.grats.model.entity.funds.PaymentMethod;
import ro.pentalog.grats.model.repository.PaymentMethodRepository;
import ro.pentalog.grats.service.paymentmethod.PaymentMethodService;

@Service
public class PaymentMethodServiceImpl implements PaymentMethodService {

	private final PaymentMethodRepository paymentMethodRepository;

	@Autowired
	public PaymentMethodServiceImpl(final PaymentMethodRepository paymentMethodRepository) {
		this.paymentMethodRepository = paymentMethodRepository;
	}

	@Override
	public PaymentMethod create(final FundsInformationCreateForm form) {
		final PaymentMethod paymentMethod = new PaymentMethod(form.getStreetAddress(), form.getCity(),
				form.getZipCode(), form.getFirstName(), form.getSurname(), form.getNumber(), form.getExpiry(),
				form.getCvc(), form.getUserId());
		return paymentMethodRepository.save(paymentMethod);
	}

	@Override
	public Optional<PaymentMethod> findOneByUserId(final String userId) {
		return paymentMethodRepository.findOneByUserId(userId);
	}

	@Override
	public Optional<Collection<PaymentMethod>> findByUserId(final String userId) {
		return paymentMethodRepository.findByUserId(userId);
	}

}
