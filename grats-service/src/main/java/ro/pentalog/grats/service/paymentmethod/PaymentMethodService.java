package ro.pentalog.grats.service.paymentmethod;

import java.util.Collection;
import java.util.Optional;

import ro.pentalog.grats.model.entity.funds.FundsInformationCreateForm;
import ro.pentalog.grats.model.entity.funds.PaymentMethod;

public interface PaymentMethodService {

	public PaymentMethod create(FundsInformationCreateForm form);

	public Optional<PaymentMethod> findOneByUserId(String userId);

	public Optional<Collection<PaymentMethod>> findByUserId(String userId);
}
