package ro.pentalog.grats.service.profile.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.pentalog.grats.model.entity.profile.Profile;
import ro.pentalog.grats.model.entity.profile.ProfileCreateForm;
import ro.pentalog.grats.model.repository.ProfileRepository;
import ro.pentalog.grats.service.profile.ProfileService;

@Service
public class ProfileServiceImpl implements ProfileService {

	private final ProfileRepository profileRepository;

	@Autowired
	public ProfileServiceImpl(ProfileRepository profileRepository) {
		this.profileRepository = profileRepository;
	}

	@Override
	public Profile create(ProfileCreateForm form) {
		Profile profile = new Profile(form.getPhoto(), form.getUsername(), "on".equals(form.getUseRealName()),
				form.getIntroduction(), form.getDescription(), form.getUserId());
		Optional<Profile> oldProfile = profileRepository.findOneByUserId(form.getUserId());
		if(oldProfile.isPresent()){
			profile.setId(oldProfile.get().getId());
		}
		return profileRepository.save(profile);
	}

	@Override
	public Optional<Profile> findOneByUserId(String userId) {
		return profileRepository.findOneByUserId(userId);
	}

	@Override
	public Optional<Profile> findOneByUsername(String username) {
		return profileRepository.findOneByUsername(username);
	}

}
